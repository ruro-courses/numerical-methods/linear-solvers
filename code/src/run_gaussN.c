#include "systems.h"
#include "gauss.h"

void example(System *S, char *name)
{
    clearStats();
    printHeader(name);
    printSystem(S, 4, 8);
    if (solveGauss(S, maxelem))
        printSolution(S, 8);
    else
        printSystem(S, 4, 8);
    printStats();
}

int main(void)
{
    System *S1 = makeSystemN(
            #include "SN1.c"
            );
    System *S2 = makeSystemN(
            #include "SN2.c"
            );
    System *S3 = makeSystemN(
            #include "SN3.c"
            );
    System *S4 = makeSystemN(
            #include "SN4.c"
            );

    example(S1, "1");
    example(S2, "2");
    example(S3, "3");
    example(S4, "4");

    delSystem(S1);
    delSystem(S2);
    delSystem(S3);
    delSystem(S4);
}
