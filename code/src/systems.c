#include <string.h>
#include <stdio.h>

#include "systems.h"

Stats st = { 0 };

void printHeader(const char *name)
{
    int nlen = strlen(name);
    int slen = (SCREEN_WIDTH - nlen)/2;
    int odd = (nlen + 2*slen == SCREEN_WIDTH);
    printf("%.*s %.*s %.*s\n",
            slen - 1, HEADER,
            nlen, name,
            slen - odd, HEADER);
}

void clearStats(void)
{
    const Stats empty = { 0 };
    st = empty;
}

void printStats(void)
{
    printHeader("Current Stats");
    printf("%18s: %20lf %s\n", "Time", 1.0*st.clock/CLOCKS_PER_SEC, "seconds");
    printf("%18s: %20ju %s\n", "Operations", st.add_count, "addition");
    printf("%18s: %20ju %s\n", "Operations", st.mul_count, "multiplication");
    printf("%18s: %20ju %s\n", "Memory", st.get_count, "reads");
    printf("%18s: %20ju %s\n", "Memory", st.set_count, "writes");
    printf("%18s: %20ju\n", "Total Iterations", st.itr_count);
    printf("\n");
}

void printEquation(Equation *E, size_t skip, int prec)
{
    size_t lenStart;
    size_t lenEnd = 0;
    if (skip && E->len > 2*skip)
        lenStart = lenEnd = skip;
    else
        lenStart = E->len;
    for (size_t i = 0; i < lenStart; i++)
        printf("%*.*lf ", prec + 4, prec, E->lhs[i]);
    if (lenEnd)
        printf("... ");
    for (size_t i = 1; i <= lenEnd; i++)
        printf("%*.*lf ", prec + 4, prec, E->lhs[E->len-i]);
    printf("= %*.*lf\n", prec + 4, prec, E->rhs);
}

void printSystem(System *S, size_t skip, int prec)
{
    printHeader("System of Linear Equations");
    size_t lenStart;
    size_t lenEnd = 0;
    if (skip && S->len > 2*skip)
        lenStart = lenEnd = skip;
    else
        lenStart = S->len;
    for (size_t i = 0; i < lenStart; i++)
        printEquation(S->sys[i], skip, prec);
    if (lenEnd)
        printf("\t\t...\t\t\t\t\t...\t\t\t\t\t...\n");
    for (size_t i = 1; i <= lenEnd; i++)
        printEquation(S->sys[S->len-i], skip, prec);
    printf("\n");
}

void printSolution(System *S, int prec)
{
    printHeader("Solution");
    size_t lenSys = S->len;
    for (size_t i = 0; i < lenSys; i++)
        printf("x%zu = %*.*lf\n", i, prec+4, prec, S->sys[i]->rhs);
    printf("\n");
}

System *allocSystem(size_t N)
{
    System *S = malloc(sizeof(*S) + sizeof(*(S->sys))*N);
    for (size_t i = 0; i < N; i++)
    {
        S->sys[i] = malloc(sizeof(*(S->sys[i])) + sizeof(*(S->sys[i]->lhs))*N);
        S->sys[i]->len = N;
    }
    S->len = N;
    return S;
}

void delSystem(System *S)
{
    size_t N = S->len;
    for (size_t i = 0; i < N; i++)
        free(S->sys[i]);
    free(S);
}


System *makeSystemF(size_t N, M matrix_gen, e vector_gen)
{
    System *S = allocSystem(N);
    for (size_t i = 0; i < N; i++)
    {
        S->sys[i]->rhs = vector_gen(N, i);
        for (size_t j = 0; j < N; j++)
            S->sys[i]->lhs[j] = matrix_gen(N, i, j);
    }
    return S;
}

System *makeSystemN(size_t N, double matrix_dat[], double vector_dat[])
{
    System *S = allocSystem(N);
    for (size_t i = 0; i < N; i++)
    {
        S->sys[i]->rhs = vector_dat[i];
        for (size_t j = 0; j < N; j++)
            S->sys[i]->lhs[j] = matrix_dat[i*N + j];
    }
    return S;
}
