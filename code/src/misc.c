#include <math.h>
#include "misc.h"

double maxNorm(System *S)
{
    size_t N = S->len;
    double maxElem = 0.0;
    for (size_t i = 0; i < N; i++)
        for (size_t j = 0; j < N; j++)
        {
            double tmp = fabs(getA(S, i, j));
            maxElem = (maxElem > tmp) ? maxElem : tmp;
        }
    return maxElem;
}

double infNorm(System *S)
{
    size_t N = S->len;
    double maxCol = 0.0;
    for (size_t i = 0; i < N; i++)
    {
        double tmp = 0.0;
        for (size_t j = 0; j < N; j++)
            tmp = add_c(tmp, fabs(getA(S, j, i)));
        maxCol = (maxCol > tmp) ? maxCol : tmp;
    }
    return maxCol;
}

double firstNorm(System *S)
{
    size_t N = S->len;
    double maxRow = 0.0;
    for (size_t i = 0; i < N; i++)
    {
        double tmp = 0.0;
        for (size_t j = 0; j < N; j++)
            tmp = add_c(tmp, fabs(getA(S, i, j)));
        maxRow = (maxRow > tmp) ? maxRow : tmp;
    }
    return maxRow;
}

double frobeniusNorm(System *S)
{
    size_t N = S->len;
    double sum = 0.0;
    for (size_t i = 0; i < N; i++)
        for (size_t j = 0; j < N; j++)
        {
            double tmp = getA(S, i, j);
            sum = add_c(sum, mul_c(tmp, tmp));
        }
    return sqrt(sum);
}
