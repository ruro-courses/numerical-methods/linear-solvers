#include <stdio.h>

#include "gauss.h"

size_t nonzero(System *S, size_t j)
{
    size_t N = S->len;
    double tmp;
    for (size_t i = j; i < N; i++)
        if ((tmp = getA(S, i, j)) != 0.0) 
            return i;
    return j;
}

size_t maxelem(System *S, size_t j)
{
    double tmp;
    size_t N = S->len;
    double max = getA(S, j, j);
    size_t ret = j;
    for (size_t i = j+1; i < N; i++)
        if ((tmp = fabs(getA(S, i, j))) > max)
        {
            max = tmp;
            ret = i;
        }
    return ret;
}

bool solveGauss(System *S, selector sel)
{
    clock_t start = clock();
    size_t N = S->len;
    for (size_t i = 0; i < N; i++)
    {
        st.itr_count++;
        size_t swap = sel(S, i);
        if (swap != i)
            swapEquations(S, i, swap);
        if(!normalize(S, i))
        {
            printHeader("Error");
            printf("Couldn't find solution, possibly a singular system.\n");
            st.clock += clock() - start;
            return false;
        }
        for (size_t j = i+1; j < N; j++)
        {
            double coeff = getA(S, j, i);
            for (size_t k = i; k < N; k++)
            {
                double tmp = getA(S, i, k);
                tmp = mul_c(tmp, coeff);
                tmp = add_c(getA(S, j, k), -tmp);
                setA(S, tmp, j, k);
            }
            double tmp = getb(S, i);
            tmp = mul_c(tmp, coeff);
            tmp = add_c(getb(S, j), -tmp);
            setb(S, tmp, j);
        }
    }
    for (size_t i = 1; i < N; i++)
    {
        st.itr_count++;
        double elem = getb(S, N-i);
        for (size_t j = i+1; j <= N; j++)
        {
            double coeff = getA(S, N-j, N-i);
            setA(S, 0.0, N-j, N-i);
            double tmp = mul_c(elem, coeff);
            tmp = add_c(getb(S, N-j), -tmp);
            setb(S, tmp, N-j);
        }
    }
    st.clock += clock() - start;
    return true;
}
