#include <math.h>

#include "relax.h"

#include <stdio.h>

bool iterRelax(System *S, double omega, double *step)
{
    size_t N = S->len;
    for (size_t i = 0; i < N; i++)
    {
        double sigma = 0;
        for (size_t j = 0; j < N; j++)
            if (i != j)
            {
                // sigma += A[i][j] * step[j]
                double tmp = mul_c(getA(S, i, j), step[j]);
                sigma = add_c(sigma, tmp);
            }
        // step[i] += omega*((b[i] - sigma)/A[i][i] - step[i])
        double tmp = add_c(getb(S, i), -sigma);
        tmp = add_c(div_c(tmp, getA(S, i, i)), -step[i]);
        step[i] = add_c(step[i], mul_c(omega, tmp));
        if (!isfinite(step[i]))
            return false;
    }
    return true;
}

double vec_diff(size_t N, double *v1, double *v2)
{
    double accum = 0.0;
    for (size_t i = 0; i < N; i++)
    {
        double tmp = add_c(v1[i], -v2[i]);
        accum += mul_c(tmp, tmp);
    }
    return sqrt(accum);
}

void vec_init(size_t N, double *v)
{
    for (size_t i = 0; i < N; i++)
        v[i] = 0;
}

void vec_cpy(size_t N, double *dst, double *src)
{
    for (size_t i = 0; i < N; i++)
        dst[i] = src[i];
}

bool solveRelax(System *S, double omega, double precision)
{
    clock_t start = clock();
    size_t N = S->len;
    double step[N];
    double last[N];
    double step_len = 0.0;
    size_t counter = 0;
    vec_init(N, step);
    do
    {
        counter++;
        st.itr_count++;
        vec_cpy(N, last, step);
        if (!iterRelax(S, omega, step) || !isfinite(step_len))
        {
            printHeader("Error");
            printf("Method doesn't converge.\n");
            st.clock += clock() - start;
            return false;
        }
        if (!(counter%500))
            printf("Iteration #%zu, ||xi - xi+1|| = %lf. "
                   "If this takes too long, the method may be divirging.\n",
                   counter, step_len);
    } while ((step_len = vec_diff(N, last, step)) > precision/2);
    
    for (size_t i = 0; i < N; i++)
        setb(S, step[i], i);
    st.clock += clock() - start;
    return true;
}
