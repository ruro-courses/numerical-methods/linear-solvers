#include "systems.h"
#include "gauss.h"

#include <stdio.h>

#include "SF.c"

int main(void)
{
    size_t N;
    printf("Please, input the size of Matrix:\n");
    scanf("%zu", &N);
    printf("Please, input the parameter M:\n");
    scanf("%lf", &paramM);
    printf("Please, input the parameter N:\n");
    scanf("%lf", &paramN);

    System *S = makeSystemF(N, genMatrix, genVector);

    clearStats();
    printHeader("Generated System");
    printSystem(S, 4, 8);
    if (solveGauss(S, maxelem))
        printSolution(S, 8);
    else
        printSystem(S, 4, 8);
    printStats();

    delSystem(S);
}
