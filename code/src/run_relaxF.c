#include "systems.h"
#include "relax.h"

#include <stdio.h>

#include "SF.c"

int main(void)
{
    size_t N;
    double omega;
    double precision;
    printf("Please, input the size of Matrix:\n");
    scanf("%zu", &N);
    printf("Please, input the parameter M:\n");
    scanf("%lf", &paramM);
    printf("Please, input the parameter N:\n");
    scanf("%lf", &paramN);
    printf("Please, input the iteration parameter:\n");
    scanf("%lf", &omega);
    printf("Please, input the desiered precision:\n");
    scanf("%lf", &precision);

    System *S = makeSystemF(N, genMatrix, genVector);

    clearStats();
    printHeader("Generated System");
    size_t digits = log10(precision);
    printSystem(S, 4, digits);
    if (solveRelax(S, omega, precision))
        printSolution(S, digits);
    printStats();

    delSystem(S);
}
