#include <stdio.h>

#include "systems.h"
#include "relax.h"

void example(System *S, char *name, double omega, double precision)
{
    clearStats();
    printHeader(name);
    size_t digits = log10(1/precision);
    printSystem(S, 4, digits);
    if (solveRelax(S, omega, precision))
        printSolution(S, digits);
    printStats();
}

int main(void)
{
    System *S1 = makeSystemN(
            #include "SN1.c"
            );
    System *S2 = makeSystemN(
            #include "SN2.c"
            );
    System *S3 = makeSystemN(
            #include "SN3.c"
            );
    System *S4 = makeSystemN(
            #include "SN4.c"
            );

    double omega, precision;
    printf("Please input the iteration parameter:\n");
    scanf("%lf", &omega);
    printf("Please input the desired precision:\n");
    scanf("%lf", &precision);

    example(S1, "1", omega, precision);
    example(S2, "2", omega, precision);
    example(S3, "3", omega, precision);
    example(S4, "4", omega, precision);

    delSystem(S1);
    delSystem(S2);
    delSystem(S3);
    delSystem(S4);
}
