// Extra parameters
double paramM = 0.0;
double paramN = 0.0;

// Matrix
M genMatrix;
double genMatrix(size_t N, size_t i, size_t j)
{
    // Counting from 1.
    i++;
    j++;
    if (i == j)
        return paramN + paramM*paramM + j/paramM + i/paramN;
    return (i+j)/(paramM+paramN);
}

// Vector
e genVector;
double genVector(size_t N, size_t i)
{
    // Counting from 1.
    i++;
    return paramM*paramN - i*i*i;
}
