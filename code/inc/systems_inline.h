inline double getA(System *S, size_t i, size_t j)
{
    st.get_count++;
    return S->sys[i]->lhs[j];
}

inline void setA(System *S, double f, size_t i, size_t j)
{
    st.set_count++;
    S->sys[i]->lhs[j] = f;
}

inline double getb(System *S, size_t i)
{
    st.get_count++;
    return S->sys[i]->rhs;
}

inline void setb(System *S, double f, size_t i)
{
    st.set_count++;
    S->sys[i]->rhs = f;
}

inline void swapEquations(System *S, size_t i, size_t j)
{
    Equation *tmp = S->sys[i];
    S->sys[i] = S->sys[j];
    S->sys[j] = tmp;
}

inline bool normalize(System *S, size_t i)
{
    double elem = getA(S, i, i);
    double temp[S->len];
    size_t N = S->len;
    for (size_t j = i; j < N; j++)
    {
        double tmp = getA(S, i, j);
        temp[j] = div_c(tmp, elem);
        if (!isfinite(temp[j]))
            return false;
    }
    double tmp = getb(S, i);
    tmp = div_c(tmp, elem);
    if (!isfinite(tmp))
        return false;
    setb(S, tmp, i);
    for (size_t j = i; j < N; j++)
        setA(S, temp[j], i, j);
    return true;
}

inline double add_c(double a, double b)
{
    st.add_count++;
    return a+b;
}

inline double mul_c(double a, double b)
{
    st.mul_count++;
    return a*b;
}

inline double div_c(double a, double b)
{
    // For now, count division as multiplication.
    st.mul_count++;
    return a/b;
}
