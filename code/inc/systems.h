#ifndef _SYSTEMS_H
#define _SYSTEMS_H
// ------------------------------- Include section ----------------------------

#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>

// ------------------------------- Define section -----------------------------

#ifndef HEADER_SEP
#define HEADER_SEP "="
#endif

#ifndef SCREEN_WIDTH
#define SCREEN_WIDTH 80
#endif

#define REP(c) c c c c c c c c c c c c c c c c
#define HEADER REP(REP(HEADER_SEP))

// ------------------------------- Type section -------------------------------

// A single linear equation of form
// lhs[0]x(0) + lhs[1]x(1) + ... + lhs[len-1]x(len-1) = rhs.
typedef struct
{
    size_t len;
    double rhs;
    double lhs[];
} Equation;

// A system of linear equations Ax = b.
typedef struct
{
    size_t len;
    Equation *sys[];
} System;

// Global state segment for timing and performance comparison.
typedef struct
{
    clock_t clock;
    size_t itr_count;
    size_t mul_count;
    size_t add_count;
    size_t get_count;
    size_t set_count;
} Stats;
extern Stats st;

// Types for functional definition of systems of linear equations.
typedef double M(size_t, size_t, size_t);
typedef double e(size_t, size_t);

// ------------------------------- Declaration section ------------------------

void printEquation(Equation *, size_t, int) __attribute__((nonnull));
void printSystem(System *, size_t, int) __attribute__((nonnull));
void printSolution(System *, int) __attribute__((nonnull));
void printHeader(const char *) __attribute__((nonnull));
void clearStats(void);
void printStats(void);
void delSystem(System *) __attribute__((nonnull));
System *allocSystem(size_t);
System *makeSystemF(size_t, M, e) __attribute__((nonnull));
System *makeSystemN(size_t, double [], double []) __attribute__((nonnull));

// ------------------------------- Inline section -----------------------------

inline double getA(System *, size_t, size_t)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline void setA(System *, double, size_t, size_t)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline double getb(System *, size_t)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline void setb(System *, double, size_t)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline void swapEquations(System *, size_t, size_t)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline bool normalize(System *, size_t)
    __attribute__((always_inline))
    __attribute__((nonnull));

inline double mul_c(double, double)
    __attribute__((always_inline));

inline double div_c(double, double)
    __attribute__((always_inline));

inline double add_c(double, double)
    __attribute__((always_inline));

#include "systems_inline.h"

#endif
