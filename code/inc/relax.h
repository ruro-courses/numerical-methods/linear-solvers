#ifndef _RELAX_H
#define _RELAX_H
// ------------------------------- Include section ----------------------------

#include "systems.h"

// ------------------------------- Declaration section ------------------------

bool solveRelax(System *, double, double) __attribute__((nonnull));

#endif
