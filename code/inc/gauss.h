#ifndef _GAUSS_H
#define _GAUSS_H
// ------------------------------- Include section ----------------------------

#include "systems.h"

// ------------------------------- Type section -------------------------------

typedef size_t selector(System *, size_t) __attribute__((nonnull));

// ------------------------------- Declaration section ------------------------

bool solveGauss(System *, selector) __attribute__((nonnull));
selector nonzero;
selector maxelem;

#endif
