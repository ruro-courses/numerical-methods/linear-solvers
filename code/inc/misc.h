#ifndef _MISC_H
#define _MISC_H
// ------------------------------- Include section ----------------------------

#include "systems.h"

// ------------------------------- Type section -------------------------------

typedef double matNorm(System *);

// ------------------------------- Declaration section ------------------------

matNorm frobeniusNorm;
matNorm firstNorm;
matNorm infNorm;
matNorm maxNorm;

#endif
